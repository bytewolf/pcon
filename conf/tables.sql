BEGIN TRANSACTION;
CREATE TABLE users (
        userid CHAR(32) PRIMARY KEY, 
        name VARCHAR(32), 
        passwd CHAR(16), 
        phone CHAR(16), 
        occupation TEXT, 
        profile TEXT, 
        date VARCHAR(16), 
        ipaddr VARCHAR(16), 
        attr VARCHAR(16) DEFAULT 'audience', 
        super INT(1) DEFAULT 0
);
INSERT INTO "users" VALUES('pconadmin@perlchina.org', 'Pconadmin', 'af15d5fdacd5fdfea300e88a8e253e82', '333333', 'xxx', 'YAPC::Beijing 管理员aaa
', '2007-01-27', '127.0.0.1', 'speaker', 1);
INSERT INTO "users" VALUES('bytewolf@gmail.com', 'hui', 'af15d5fdacd5fdfea300e88a8e253e82', '1234567', 'IT', '我是PERL爱好者哦', '2007-02-04', '127.0.0.1', 'volunteer', 0);
CREATE TABLE pages (
        title VARCHAR(64) PRIMARY KEY,
        content TEXT, 
        datetime VARCHAR(32) NOT NULL,
        user VARCHAR(32) NOT NULL,
        trashed INT(1) DEFAULT 0
);
INSERT INTO "pages" VALUES('about_yapc', 'h1=. YAPC::Beijing ^2007^ 大会简介

<hr>
主办单位： "中国Perl协会":http://www.perlchina.org
承办单位： 北京Perl推广组
媒体协办： "China Unix":http://www.chinaunix.net 、 "Yahoo！中国":http://www.yahoo.com.cn 、 "搜狐":http://www.sohu.com 、 "网易":http://www.163.com 、 "新浪":http://www.sina.com.cn
赞助单位： Open Vox 、 "Yahoo！中国":http://www.yahoo.com.cn


h2. 你期盼已久的YAPC::Beijing ^2007^ 终于来了！

你准备好了吗？2007年 5 月 19 日至 20 日，世界级Perl开源程序员会议首次登陆北京！
此次由中国Perl协会主办，Perl北京推广组协办的YAPC::Beijing 2007，精彩多多，不容错过！

今年大会在议题上有什么新亮点呢？此次YAPC::Beijing ^2007^ 大会以“直面Perl界热点人物，跟随技术最前沿”为口号，精心准备了如下演讲大餐：

* 我们的老朋友，Perl界知名人士、“台湾十大电脑高手”唐凤女士（原名“唐宗汉”）又回来了！自从05年Perl China年度大会上刮起一阵Pugs风暴之后，场面亦不可收拾，而此次唐凤女士也誓将Pugs风暴进行到底！

h3. 当然，YAPC::Beijing ^2007^ 的精彩还远不仅如此：

* 举办地选址校园，让我们感受来自大学校园的学术冲击，亦让我们回到那个曾经美好的菁菁岁月。

* 破冰环节“Get To know”，让我们一起创意在线，大会主题贴纸涂鸦与Perl Monks诗句大汇，无数开源英雄的理念释放。

* 十余场演讲风暴，分享技术，交流经验，让我们一起打造Perl最明亮的应用前景。

* 集体午餐“以餐会友”—按照兴趣、爱好划分坐席，认识跟自己臭味相投的界内朋友。

* 会议晚宴 —“ YAPC 之夜“ ，只要你愿意，就可以和Perl界名人一起共进晚餐哦！

* 国内Perl商业应用企业大云集，合作伙伴携手共赢。也许到了你该跳槽的时候了！

* YAPC::Beijing 2007主题纪念品限量超值热卖，支持开源，惊喜不断。

* "开源书籍漂流活动”，一本书交无数个朋友，绵绵之力，造福大家。

* YAPC Show，技术高手的娱乐盛宴，我们共联欢！

* 闪电娱乐环节“休息时间”，点燃Perl China首次年度颁奖典礼序幕。

* 所有出席者将获得“YAPC::Beijing 2007参会者特别纪念包”一份。

云集百名技术精英，汇集数十场技术风暴，最热门新鲜的技术与最炙手可热的理念，这一切尽在Perl界最顶级的技术盛宴—YAPC::Beijing 2007，你又岂能错过？

锁定精彩，五月的北京，让我们一起向YAPC出发！ ', '2007-03-13 23:35:24', 'pconadmin@perlchina.org', 0);
INSERT INTO "pages" VALUES('contact', '*{font-weight:bold} Contact us at:
** Email: hui { at } perlchina.org
** Phone: +86 010 xxxxxxx
** Forum: "bbs.perlchina.org":http://bbs.perlchina.org
** Home : "www.perlchina.org":http://www.perlchina.org', '2007-02-07 16:48:15', 'pconadmin@perlchina.org', 0);
INSERT INTO "pages" VALUES('about_us', 'h1. YAPC::Beijing ^2007^

We are nerds love "Perl":http://www.perl.org ,We found the "PerlChina":http://www.perlchina.org
YAPC 的使命在于推动PERL在各国的友好交流和发展

YAPC 的使命在于推动PERL在各国的友好交流和发展YAPC 的使命在于推动PERL在各国的友好交流和发展YAPC 的使命在于推动PERL在各国的友好交流和发展

', '2007-02-07 16:47:27', 'pconadmin@perlchina.org', 0);
INSERT INTO "pages" VALUES('vips', 'h1=. YAPC::Beijing ^2007^ 嘉宾介绍

<hr>

!(speaker)http://tokyo.yapcasia.org/speakers/photos/audrey_tang.jpg! 

h2<. 唐凤

* _Web_ ： "http://pugs.blogs.com":http://pugs.blogs.com
* _From_ : TaiWan
* _Profile_ :

p{text-indent:2em}. 唐凤是来自台湾的一名真正的 _HACKER_
fdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafds
afdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafds
afdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsafdsa

* 他将演讲的题目有:
** HASKELL
** Jifty

<hr> <!-- 此为每个演讲者资料的分隔线 -->

!(speaker)http://tokyo.yapcasia.org/speakers/photos/larry_wall.jpg!  

h2<. Larry Wall

* _Web_ ： "http://www.wall.org/~larry/":http://www.wall.org/~larry/
* _From_ : TaiWan
* _Profile_ :

p{text-indent:2em}. Larry Wall 乃 Perl 之父!
fdsafdsafdsafdsafsafdsa
fdsafdsafdsaasfdsafdsafdsafdsa
fdsafdsafdsafdsafdsafsdafsafdsafsa

* 他将演讲的题目有:
** "Perl":http://perl.org', '2007-02-07 15:32:53', 'pconadmin@perlchina.org', 0);
INSERT INTO "pages" VALUES('schedule', '|a|b|c|
{background:#ddd}.|d|e|f|
|g|h|i|', '2007-02-07 16:46:37', 'pconadmin@perlchina.org', 0);
INSERT INTO "pages" VALUES('talks', '整理征集中....', '2007-03-16 23:11:25', 'pconadmin@perlchina.org', 0);
INSERT INTO "pages" VALUES('sponsors', 'Perlchina
Yahoo!', '2007-02-07 16:47:02', 'pconadmin@perlchina.org', 0);
INSERT INTO "pages" VALUES('location', '北京大学某大厅', '2007-02-05 13:48:26', 'bytewolf@gmail.com', 0);
CREATE TABLE passwords (
        userid VARCHAR(32) PRIMARY KEY,
        remind_pwd VARCHAR(32),
        epoch VARCHAR(16) NOT NULL
);

CREATE TABLE upload (
	filename VARCHAR(64) PRIMARY KEY,  
	title VARCHAR(64),
	description TEXT,
	filepath VARCHAR(256) NOT NULL, 
	datetime VARCHAR(32) NOT NULL,
	userid VARCHAR(32)
);

CREATE TABLE announcement (
id VARCHAR(1) PRIMARY KEY,
datetime varchar(32),
content TEXT,
userid VARCHAR(32),
name varchar(32)
);

CREATE TABLE news (
datetime varchar(32) primary key,
title varchar(128),
content text,
userid varchar(32),
name varchar(32)
);
COMMIT;
