package Pcon::Conf;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use base qw/Exporter/;
use vars qw/@EXPORT/;
@EXPORT = qw/get_conf get_runmodes/;
my $conf_file = "pcon.conf";

sub get_runmodes {
    my $run_modes = {
### Configure run modes here ###
        'Pcon' => [
            qw/
              view
              /
        ],

        'Pcon::News' => [
            qw/
              list
              /
        ],

        'Pcon::C::Users' => [
            qw/
              reg  logout  login  list view
              _manage _edit_passwd
              /
        ],

        'Pcon::C::CAPTCHA' => [
            qw/
              gen
              /
        ],

#       'Pcon::C::Pages' => [
#          qw/
#              _create  _edit _trash
#              /
#        ],

        'Pcon::C::Files' => [
            qw/
              _manage  _upload  _edit _del
              /
        ],
        
        'Pcon::C::Admin' => [
        	qw/
        	_frame _menu
        	/
        ],

        'Pcon::C::Admin::Users' => [
        	qw/
        	hello list
        	_panel _del _super _unsuper _getemailaddrs
        	/
        ],

        'Pcon::C::Admin::News' => [
        	qw/
        	hello 
        	_panel _announcement _add _del _edit _view
        	/
        ],

        'Pcon::C::Admin::Pages' => [
        	qw/
        	hello 
        	_panel _view _create  _edit _trash 
        	_del _restatic_all _revert
        	/
        ],

        'Pcon::C::Admin::Files' => [
        	qw/
        	hello _panel
        	/
        ],        
        

### END ###
    };
    my %modes = ();
    return [grep { !$modes{$_}++ } map { @{$_} } values %{$run_modes}];
}

sub get_conf {
    #############
    # Pcon configure file
    #############
    my $self = shift;
    my $CONF = abs_path("$FindBin::Bin/../conf") . "/$conf_file";
    $self->config_file($CONF);
    my $app_path = $self->config_param('Pcon')->{'AppPath'};
    $self->{'conf'}->{'lib'} = $app_path . 'lib/';
    $self->{'conf'}->{'web'}->{'home'} =
      $self->config_param('Pcon')->{'Web'}->{'HomeURL'};
    $self->{'conf'}->{'web'}->{'static'} =
      $app_path . $self->config_param('Pcon')->{'Web'}->{'Static'};
    $self->{'conf'}->{'web'}->{'pages'} =
      $app_path . $self->config_param('Pcon')->{'Web'}->{'Pages'};
    $self->{'conf'}->{'web'}->{'upload'} =
      $app_path . $self->config_param('Pcon')->{'Web'}->{'Upload'};
    $self->{'conf'}->{'web'}->{'uploadmaxsize'} =
      eval $self->config_param('Pcon')->{'Web'}->{'UploadMaxSize'};
    $self->{'conf'}->{'web'}->{'templates'} =
      $app_path . $self->config_param('Pcon')->{'Web'}->{'TemplateRoot'};
    my $data_path = $self->config_param('Pcon')->{'Database'};
    $self->{'conf'}->{'data'}->{'driver'}   = $data_path->{'Driver'};
    $self->{'conf'}->{'data'}->{'file'}     = $app_path . $data_path->{'File'};
    $self->{'conf'}->{'data'}->{'host'}     = $data_path->{'Host'};
    $self->{'conf'}->{'data'}->{'user'}     = $data_path->{'User'};
    $self->{'conf'}->{'data'}->{'password'} = $data_path->{'Password'};
    my $mailer = $self->config_param('Pcon')->{'Mailer'};
    $self->{'conf'}->{'mail'}->{'smtp'} = $mailer->{'Smtp'};
    $self->{'conf'}->{'mail'}->{'port'} = $mailer->{'Port'};
    $self->{'conf'}->{'mail'}->{'from'} = $mailer->{'From'};
    $self->{'conf'}->{'mail'}->{'charset'} = $mailer->{'Charset'};
    
    return $self->{'conf'};
}

1;
