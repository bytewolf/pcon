package Pcon::M::DBIx;

use strict;
use base qw/Exporter DBIx::Abstract/;
use vars qw/@EXPORT/;
@EXPORT = qw/get_dsn/;

sub get_dsn {
    my $self = shift;

    #############
    # DSN configure
    #############
    my $db_conf = $self->{'conf'}->{'data'};
    my $dsn     = {
        driver   => $db_conf->{'driver'},
        host     => $db_conf->{'host'},
        dbname   => $db_conf->{'file'},
        user     => $db_conf->{'user'},
        password => $db_conf->{'password'},
    };

    return __PACKAGE__->connect($dsn);
}

1;
