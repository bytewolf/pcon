package Pcon::Dispatch;
use base 'CGI::Application::Dispatch';
use CGI::Carp qw/fatalsToBrowser/;

$ENV{PATH_INFO} = lc $ENV{PATH_INFO} if defined $ENV{PATH_INFO};

sub dispatch_args {
        return {
            prefix  => '',
            not_found => '/html/uri_not_found.html',
            table   => [
                'admin/:app/:rm/:var?' => {prefix=>'Pcon::C::Admin'},
                ':app/:rm/:var?' => {prefix=>'Pcon::C'}, 
	           	':rm' => {app=>'Pcon'},
                '' => {app=>'Pcon'},
            ],
        };
    }
    
 1;
