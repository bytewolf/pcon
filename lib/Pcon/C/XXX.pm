package Pcon::C::Pages;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use Text::Textile qw/textile/;
use base 'Pcon';

my $p = new Text::Textile(char_encoding => 0);
sub view {
	my $self = shift;
	my $q = $self->query();
	my $path = $self->{'conf'}->{'web'}->{'text'};
	my $file = $self->param('var');
	if (-e $path.$file) {
		open(FILE,"<".$path.$file) or die "Can\'t open file $file: $!\n";		
		local $/=undef;
		my $content = <FILE>;
		close FILE;
		my $t = $self->load_tmpl('pages_view.tmpl');
		$t->param(
			HOME => $self->{'conf'}->{'web'}->{'home'},
			TITLE => "View $file",
			FILENAME => $file,
			CONTENT => $p->textile($content),
		);
		return $t->output;
	}
	else {
		$self->redirect($self->{'conf'}->{'web'}->{'home'}."/pages/_create/$file");
	}
}


sub _create {
	my $self = shift;
	my $q = $self->query();
	my $path = $self->{'conf'}->{'web'}->{'text'};
	my $file = $self->param('var');
	if (-e $path.$file) {
		return $self->args_error("$file  exist !");
	} 
	elsif ($q->param('do_edit')) {
		open(FILE,">".$path.$file) or die "Can\'t create file $file: $!\n";
		print FILE $q->param('content');
		close FILE;
		$self->redirect($self->{'conf'}->{'web'}->{'home'}."/pages/view/$file");
	} 
	else {
		my $t = $self->load_tmpl('pages_edit_or_create.tmpl');
		$t->param( 
			ACTION => '_create',
			HOME => $self->{'conf'}->{'web'}->{'home'},
			TITLE => "Create $file",
			FILENAME => $file,
			CONTENT => 'New content here!',
		);
		return $t->output;
	}
}


sub _edit {
	my $self = shift;
	my $q = $self->query();
	my $path = $self->{'conf'}->{'web'}->{'text'};
	my $file =$self->param('var');
	if (-e $path.$file && $q->param('do_edit')) {
		open(FILE,"+>".$path.$file) or die "Can\'t edit file $file: $!\n";
		print FILE $q->param('content');
		close FILE;
		$self->redirect($self->{'conf'}->{'web'}->{'home'}."/pages/view/$file");
	}
	elsif (-e $path.$file && not $q->param('do_edit')) {
		local $/=undef;
		open(FILE,"+<".$path.$file,) or die "Can\'t open file $file: $!\n";
		my $content = <FILE>;
		close FILE;
		my $t = $self->load_tmpl("pages_edit_or_create.tmpl");
		$t->param( 
			ACTION => '_edit',
			HOME => $self->{'conf'}->{'web'}->{'home'},
			TITLE => "Edit $file",
			FILENAME => $file,
			CONTENT => $content,
		);
		return $t->output;	
	}
}


1;
