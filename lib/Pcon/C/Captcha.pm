package Pcon::C::Captcha;

use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'Pcon';

sub gen {

    my $self = shift;
    $self->captcha_config(
        IMAGE_OPTIONS => {
            width  => 70,
            height => 25,
            lines  => 1,
            font => $self->{'conf'}->{'lib'} . "/fonts/FreeMonoBoldOblique.ttf",
            ptsize   => 18,
            bgcolor  => "#FFFFFF",
            rndmax   => 4,
            rnd_data => [ A .. Z ],
        },
        CREATE_OPTIONS   => [ 'ttf', 'rect' ],
        PARTICLE_OPTIONS => [400],
    );
    return $self->captcha_create;
}

1;
