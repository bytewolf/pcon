package Pcon::C::Pages;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use Text::Textile qw/textile/;
use base 'Pcon';

my $p = new Text::Textile( char_encoding => 0 );

sub view {
    my $self       = shift;
    my $q          = $self->query();
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $file       = lc $self->param('var');
    $file =~ s/\s/_/g;
    $file =~ s/\..*$//g;
    if ( -e $pages_path . $file ) {
        open( FILE, "<" . $pages_path . $file )
          or die "Can\'t open file $file: $!\n";
        local $/ = undef;
        my $content = <FILE>;
        close FILE;
        my $t = $self->load_tmpl('pages_view.tmpl');
        $t->param(
            HOME     => $self->{'conf'}->{'web'}->{'home'},
            TITLE    => "View $file",
            FILENAME => $file,
            CONTENT  => $p->textile($content),
        );
        return $t->output;
    }
    else {
        $self->redirect(
            $self->{'conf'}->{'web'}->{'home'} . "/pages/_create/$file" );
    }
}

sub _create {
    my $self       = shift;
    my $q          = $self->query();
    my $db         = $self->{'dsn'};
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $file       = lc $self->param('var');
    $file =~ s/\s+/_/g;
    $file =~ s/\..*$//;
    if ( -e $pages_path . $file . '.html' ) {
        return $self->args_error(
"$file.html  exist !<a href=\"$self->{'conf'}->{'web'}->{'home'}/pages/_edit/$file\">Edit?</a><br><a href=\"javascript:history.go(-1);\"><--- 返回</a>"
        );
    }
    elsif ( $q->param('do_edit_or_create') ) {
        $db->replace(
            'pages',
            {
                title    => $file,
                content  => $q->param('content'),
                datetime => $self->now('datetime'),
                user     => $self->authen->username,
            }
        );
        $self->do_static( $file, $q->param('content') );
        $self->redirect("/pages/$file.html");
    }
    else {
        my $t = $self->load_tmpl('pages_edit_or_create.tmpl');
        $t->param(
            ACTION   => '_create',
            HOME     => $self->{'conf'}->{'web'}->{'home'},
            TITLE    => "Create $file",
            FILENAME => $file,
            CONTENT  => 'New content here!',
        );
        return $t->output;
    }
}

sub _edit {
    my $self       = shift;
    my $q          = $self->query();
    my $db         = $self->{'dsn'};
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $file       = lc $self->param('var');
    $file =~ s/\s+/_/g;
    $file =~ s/\..*$//;

    if ( -e $pages_path . $file . '.html' && $q->param('do_edit_or_create') ) {
        $db->update(
            'pages',
            {
                title    => $file,
                content  => $q->param('content'),
                datetime => $self->now('datetime'),
                user     => $self->authen->username,
            },
            { title => $file },
        );
        $self->do_static( $file, $q->param('content') );
        $self->redirect("/pages/$file.html");
    }
    elsif ( -e $pages_path . $file . '.html'
        && not $q->param('do_edit_or_create') )
    {
        my $result =
          $db->select_one_to_hashref( '*', 'pages', { title => $file } );
        my $t = $self->load_tmpl("pages_edit_or_create.tmpl");
        $t->param(
            ACTION   => '_edit',
            HOME     => $self->{'conf'}->{'web'}->{'home'},
            TITLE    => "Edit $file",
            FILENAME => $file,
            CONTENT  => $result->{'content'},
        );
        return $t->output;
    }
    return $self->args_error( 'File not exist! <a href="'
          . "$self->{'conf'}->{'web'}->{'home'}/pages/_create/$file"
          . '">Create?</a><br><a href="javascript:history.go(-1);"><--- 返回</a>'
    );
}

sub _trash {
    my $self        = shift;
    my $db          = $self->{'dsn'};
    my $pages_path  = $self->{'conf'}->{'web'}->{'pages'};
    my $trashs_path = $self->{'conf'}->{'web'}->{'trashs'};
    my $file        = lc $self->param('var');
    $file =~ s/\..*$//;

    if ( -e $pages_path . $file . '.html' ) {
        unlink( $pages_path . $file . '.html' )
          or die "Can\'t delete file ${pages_path}${file}.html: $!\n";
        $db->update( 'pages', { trashed => 1, }, { title => $file }, );

        #		my $t = $self->load_tmpl('pages_del_ok.tmpl');

        return 'ok';
    }
    else {
        return $self->args_error(
'File not exist!<br><a href="javascript:history.go(-1);"><--- 返回</a>'
        );
    }
}

sub do_static {
    my ( $self, $file, $content ) = @_;
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $t          = $self->load_tmpl('pages_do_static.tmpl');
    my $username   = $self->authen->username;
    $username =~ s/\@.*//;
    $t->param(
        HOME                => $self->{'conf'}->{'web'}->{'home'},
        TITLE               => "View $file",
        FILENAME            => $file,
        CONTENT             => $p->textile($content),
        CREATE_OR_EDIT_USER => $self->now('datetime') . ' by ' . $username,
    );
    open( FILE, ">$pages_path$file.html" )
      or die "Can\'t open file $file: $!\n";
    print FILE $t->output;
    close FILE;
}

1;
