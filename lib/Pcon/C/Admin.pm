package Pcon::C::Admin;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'Pcon';

sub _frame {
	my $self = shift;
	my $db = $self->{'dsn'};
    my $result = $db->select_one_to_hashref( '*', 'users',{ userid => $self->authen->username } );
    
	if ($result->{'super'}) {
		my $t = $self->load_tmpl('admin_frame.tmpl');
		$t->param(
			HOME     => $self->{'conf'}->{'web'}->{'home'},
		);
		return $t->output;
	}
	else{
		return $self->args_error('您不是管理员！<br><a href="javascript:history.go(-1);"><--- 返回</a>');		
	}
}


sub _menu {
	my $self = shift;
	my $t = $self->load_tmpl('admin_menu.tmpl');
	$t->param(
		HOME     => $self->{'conf'}->{'web'}->{'home'},
	);
	return $t->output;
}


1;
