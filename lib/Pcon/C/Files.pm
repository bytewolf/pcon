package Pcon::C::Files;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'Pcon';

sub _manage {
    my $self   = shift;
    my $db     = $self->{'dsn'};
    my $result =
      $db->select_one_to_hashref( '*', 'users',
        { userid => $self->authen->username } );
    my $is_super   = $result->{'super'};
    my $is_speaker = $result->{'attr'} eq 'speaker';
    my $t          = $self->load_tmpl('files_manage.tmpl');
    $t->param(
        HOME                => $self->{'conf'}->{'web'}->{'home'},
        IS_SPEAKER_OR_SUPER => $is_speaker || $is_super,
        USERID              => $self->authen->username,
    );
    return $t->output;
}

sub _upload {
    my $self        = shift;
    my $q           = $self->query();
    my $filename    = $q->param('filename');
    my $upload_path = $self->{'conf'}->{'web'}->{'upload'};
    my $db          = $self->{'dsn'};
    my $result      =
      $db->select_one_to_hashref( '*', 'users',
        { userid => $self->authen->username } );
    my $is_super = $result->{'super'};
    my $is_speaker = $result->{'attr'} eq 'speaker';
    my $username;
    ( $username = $self->authen->username ) =~ s/\@/_/;
    my $file_total_length = `du ${upload_path}$username* -bc| tail -1`;
    $file_total_length =~ s /\D//g;

    return $self->args_error(
'您不是演讲者或管理员,无法上传!<br><a href="javascript:history.go(-1);"><--- 返回</a>'
      )
      unless $is_speaker
      or $is_super;

    return $self->args_error(
'您的空间不够了!<br><a href="javascript:history.go(-1);"><--- 返回</a>'
      )
      if ( $ENV{'CONTENT_LENGTH'} + $file_total_length ) >
      	$self->{'conf'}->{'web'}->{'uploadmaxsize'}
      	&& not $is_super;

    my ( $results, $err_page ) =
      $self->check_rm( 'manage_form', 'manage_form_profile',
        { ignore_fields => ['check_sum'] } );
    return $err_page if $err_page;

    return $self->args_error(
'您上传的文件太大!<br><a href="javascript:history.go(-1);"><--- 返回</a>'
    ) if $q->cgi_error =~ /413/;
    return $self->args_error(
'您上传的文件名或文件类型不符合要求!<br><a href="javascript:history.go(-1);"><--- 返回</a>'
      )
      unless $filename =~
      /^[a-zA-Z0-9_]+(\.tar\.gz|\.zip|\.png|\.jpg|\.gif|\.txt)/;

    my ( $bytesread, $buffer, $totalbytes );
    open( FH, ">", $upload_path . $username . '_' . $filename )
      or die "Upload file $filename failed :$!\n";
    while ( $bytesread = read( $filename, $buffer, 1024 ) ) {
        $totalbytes += $bytesread;
        print FH $buffer;
    }
    die "Read file $filename failed: $!\n" unless defined $bytesread;
    close FH;

    my $db = $self->{'dsn'};

    $self->redirect( $self->{'conf'}->{'web'}->{'home'} . '/files/_manage' );
    return 1;
}

sub manage_form {
    my ( $self, $errs ) = @_;
    my $t = $self->load_tmpl( 'files_manage.tmpl', die_on_bad_params => 0 );
    $t->param( 
	    	HOME => $self->{'conf'}->{'web'}->{'home'},
	    	IS_SPEAKER_OR_SUPER => 1,
        	USERID              => $self->authen->username,
    	);
    $t->param($errs) if $errs;
    return $t->output;
}

sub manage_form_profile {
    my $self = shift;
    my $q    = $self->query();

    return {
        required    => [qw/title filename description check_sum/],
        filters     => ['trim'],
        constraints => {
            title       => '/^.{1,64}$/',
            filename    => '/^.{3,64}$/',
            description => sub {
                my $var = shift;
                return 1 if $var =~ /^.{1,512}$/s;
                return 0;
            },
            check_sum => sub {
                return $self->captcha_verify( $q->cookie('hash'),
                    $q->param('check_sum') );
            },
        },

    };
}

sub _edit {
    return 1;
}

1;
