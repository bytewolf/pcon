package Pcon::C::Admin::Users;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'Pcon';

sub hello {
	no strict 'subs';
	return __PACKAGE__;
}
#######################

sub _panel {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $t = $self->load_tmpl('admin_users_panel.tmpl');
	$t->param( %{$self->list()} );
	return $t->output;
}
#######################

sub list {
    my $self   = shift;
	return "你不是管理员!" unless $self->is_super; 
    my $db     = $self->{'dsn'};
    my $result = "";
    $db->select(
        {
            fields => [ 'userid', 'name', 'attr', 'super','date' ],
            table  => 'users',
            order  => 'date',
        }
    );
    $result = $db->fetchall_arrayref;

    my ( @speaker, @audience, @volunteer, @super );
    if ( ref $result eq 'ARRAY' && @{$result} ) {
        for ( @{$result} ) {

            push @super => {
                USERID => scalar reverse( $_->[0] ),
                EMAIL => $_->[0],
                NAME   => $_->[1],
                DATE => $_->[4],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              } if $_->[3];

            push @speaker => {
                USERID => scalar reverse( $_->[0] ),
                EMAIL => $_->[0],
                NAME   => $_->[1],
                IS_SUPER => $_->[3],
                DATE => $_->[4],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              }
              and next if $_->[2] eq 'speaker';

            push @audience => {
                USERID => scalar reverse( $_->[0] ),
                EMAIL => $_->[0],
                NAME   => $_->[1],
                IS_SUPER => $_->[3],
                DATE => $_->[4],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              }
              and next if $_->[2] eq 'audience';

            push @volunteer => {
                USERID => scalar reverse( $_->[0] ),
                EMAIL => $_->[0],
                NAME   => $_->[1],
                IS_SUPER => $_->[3],
                DATE => $_->[4],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              }
              and next if $_->[2] eq 'volunteer';
        }
        
    }

    return {
        SPEAKER   => \@speaker,
        AUDIENCE  => \@audience,
        VOLUNTEER => \@volunteer,
        SUPER     => \@super,
    };
}
#######################


sub _del {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $userid = $self->param('var');
	if ($userid) {
		$db->delete('users',{userid=>$userid});
		if ( $db->rows ){
			return "<script>alert('删除用户$userid成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";		
		}
		else {
			return $self->args_error('删除失败!');
		}
	}
	else {
		return $self->args_error('参数错误!');
	}
}
#######################


sub _super {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $userid = $self->param('var');
	if ($userid) {
		$db->update(
			'users',
			{
				super => 1,
			},
			{userid => $userid},
		);
		if ( $db->rows ){
			return "<script>alert('给用户$userid授权成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";		
		}
		else {
			return $self->args_error('授权失败!');
		}
	}
	else {
		return $self->args_error('参数错误!');
	}
}
#######################


sub _unsuper {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $userid = $self->param('var');
	if ($userid) {
		$db->update(
			'users',
			{
				super => 0,
			},
			{userid => $userid},
		);
		if ( $db->rows ){
			return "<script>alert('收回用户$userid管理权成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";		
		}
		else {
			return $self->args_error('操作失败!');
		}
	}
	else {
		return $self->args_error('参数错误!');
	}
}

sub _getemailaddrs {
	my $self = shift;
	my $db = $self->{'dsn'};
	return join( ',',keys %{ $db->select_all_to_hashref('userid','users') } );
}

sub _all_print {
	my $self =shift;
}


1;
