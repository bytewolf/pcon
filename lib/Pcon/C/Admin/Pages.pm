package Pcon::C::Admin::Pages;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use Text::Textile;
use base 'Pcon';

my $p = new Text::Textile( char_encoding => 0 );


sub hello {
	no strict 'subs';
	return __PACKAGE__;
}
#######################


sub _panel {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $result = "";
	$db->select(
        {
            fields => [ 'title', 'datetime','trashed' ],
            table  => 'pages',
            order  => 'datetime',
        }
    );
    $result = $db->fetchall_arrayref;
    my (@created_pages,@trashed_pages);
    if (ref $result eq 'ARRAY' && @${result}) {
	    for ( @{$result} ) {
	    	push @created_pages => {
	    		HOME => $self->{'conf'}->{'web'}->{'home'},
	    		TITLE => $_->[0],
	    		DATETIME => $_->[1],
	    	} unless $_->[2];
	    	
	    	push @trashed_pages => {
	    		HOME => $self->{'conf'}->{'web'}->{'home'},
	    		TITLE => $_->[0],
	    		DATETIME => $_->[1],
	    	} if $_->[2];
	    }
    }
	my $t = $self->load_tmpl('admin_pages_panel.tmpl');
	$t->param(
			HOME     => $self->{'conf'}->{'web'}->{'home'},
			CREATED_PAGES => \@created_pages,
			TRASHED_PAGES => \@trashed_pages,
	);
	return $t->output;
}
#######################


sub _view {
    my $self = shift;
    return "你不是管理员!" unless $self->is_super; 
    my $db = $self->{'dsn'};
    my $q = $self->query();
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $file = lc $self->param('var');
    $file =~ s/\s/_/g;
    $file =~ s/\..*$//g;
    if ( $file ) {
		my $result = $db->select_one_to_hashref(
			'*',
			'pages',
			{ title => $file },
		);
        my $content = $result->{'content'};
        my $t = $self->load_tmpl('admin_pages_view.tmpl');
        $t->param(
            HOME     => $self->{'conf'}->{'web'}->{'home'},
            TITLE    => "View $file",
            FILENAME => $file,
            CONTENT  => $p->textile($content),
        );
        return $t->output;
    }
    else {
        $self->redirect(
            $self->{'conf'}->{'web'}->{'home'} . "/admin/pages/_create/$file" 
        );
    }
}
#######################


sub _create {
    my $self       = shift;
    return "你不是管理员!" unless $self->is_super; 
    my $q          = $self->query();
    my $db         = $self->{'dsn'};
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $file       = lc($self->param('var')||$q->param('file'));
    $file =~ s/\s+/_/g;
    $file =~ s/\..*$//;
    if ( -e $pages_path . $file . '.html' ) {
        return $self->args_error(
"$file.html  exist !<br><a href=\"$self->{'conf'}->{'web'}->{'home'}/admin/pages/_edit/$file\">Edit?</a><br><a href=\"javascript:history.go(-1);\"><--- 返回</a>"
        );
    }
    elsif ( $q->param('do_edit_or_create') ) {
        $db->replace(
            'pages',
            {
                title    => $file,
                content  => $q->param('content'),
                datetime => $self->now('datetime'),
                user     => $self->authen->username,
            }
        );
        $self->do_static( $file, $q->param('content') );
		return "<script>alert('成功创建页面 $file !');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
    }
    elsif ($file) {
        my $t = $self->load_tmpl('admin_pages_edit_or_create.tmpl');
        $t->param(
            ACTION   => '_create',
            HOME     => $self->{'conf'}->{'web'}->{'home'},
            TITLE    => "Create $file",
            FILENAME => $file,
            CONTENT  => 'New content here!',
        );
        return $t->output;
    }
    else {
    	return "参数不正确!<br><a href=\"$ENV{'HTTP_REFERER'}\"><--- 返回</a>";
    }
}
#######################


sub _edit {
    my $self       = shift;
    return "你不是管理员!" unless $self->is_super; 
    my $q          = $self->query();
    my $db         = $self->{'dsn'};
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $file       = lc $self->param('var');
    $file =~ s/\s+/_/g;
    $file =~ s/\..*$//;

    if ( -e $pages_path . $file . '.html' && $q->param('do_edit_or_create') ) {
        $db->update(
            'pages',
            {
                title    => $file,
                content  => $q->param('content'),
                datetime => $self->now('datetime'),
                user     => $self->authen->username,
            },
            { title => $file },
        );
        $self->do_static( $file, $q->param('content') );
        $self->redirect("/pages/$file.html");
    }
    elsif (not (-e $pages_path . $file . '.html') && $q->param('do_edit_or_create')) {
        $db->update(
            'pages',
            {
                title    => $file,
                content  => $q->param('content'),
                datetime => $self->now('datetime'),
                user     => $self->authen->username,
            },
            { title => $file },
        );
    	$self->redirect($self->{'conf'}->{'web'}->{'home'}."/admin/pages/_view/$file");
    }
    elsif ( (my $result = $db->select_one_to_hashref( '*', 'pages', { title => $file } )) 
    			&& not (-e $pages_path . $file . '.html')
    			&&  not $q->param('do_edit_or_create')
    ) {
        my $t = $self->load_tmpl("admin_pages_edit_or_create.tmpl");
    	$t->param(
            ACTION   => '_edit',
            HOME     => $self->{'conf'}->{'web'}->{'home'},
            TITLE    => "Edit $file",
            FILENAME => $file,
            CONTENT  => $result->{'content'},
        );
        return $t->output;
    }
    elsif ( -e $pages_path . $file . '.html'
        && not $q->param('do_edit_or_create') 
    ) {
        my $result =
          $db->select_one_to_hashref( '*', 'pages', { title => $file } );
        my $t = $self->load_tmpl("admin_pages_edit_or_create.tmpl");
        $t->param(
            ACTION   => '_edit',
            HOME     => $self->{'conf'}->{'web'}->{'home'},
            TITLE    => "Edit $file",
            FILENAME => $file,
            CONTENT  => $result->{'content'},
        );
        return $t->output;
    }
    return $self->args_error( '文件不存在! <a href="'
          . "$self->{'conf'}->{'web'}->{'home'}/admin/pages/_create/$file"
          . '">Create?</a><br><a href="javascript:history.go(-1);"><--- 返回</a>'
    );
}
#######################


sub _revert {
    my $self        = shift;
    return "你不是管理员!" unless $self->is_super; 
    my $db          = $self->{'dsn'};
    my $pages_path  = $self->{'conf'}->{'web'}->{'pages'};
    my $file        = lc $self->param('var');
    $file =~ s/\..*$//;
    my $result = $db->select_one_to_hashref('content','pages',{title => $file});
    if ( $file && defined $result->{'content'} ) {
    	$self->do_static($file,$result->{'content'});
    	$db->update( 'pages', { trashed => 0, }, { title => $file }, );
		return "<script>alert('成功恢复页面 $file !');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
    }
    else {
    	$self->args_error("数据库中找不到该页!<br><a href=\"javascript:history.go(-1) \"><--- 返回</a>");
    }
 
}
#######################


sub _trash {
    my $self        = shift;
    return "你不是管理员!" unless $self->is_super; 
    my $db          = $self->{'dsn'};
    my $pages_path  = $self->{'conf'}->{'web'}->{'pages'};
    my $file        = lc $self->param('var');
    $file =~ s/\..*$//;

    if ( -e $pages_path . $file . '.html' ) {
        unlink( $pages_path . $file . '.html' )
          or die "Can\'t delete file ${pages_path}${file}.html: $!\n";
        $db->update( 'pages', { trashed => 1, }, { title => $file }, );

        #		my $t = $self->load_tmpl('admin_pages_del_ok.tmpl');
		return "<script>alert('成功回收页面 $file !');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
       # return "ok<br><a href=\"$ENV{'HTTP_REFERER'}\"><--- 返回</a>";
    }
    else {
        return $self->args_error(
'File not exist!<br><a href="javascript:history.go(-1);"><--- 返回</a>'
        );
    }
}
#######################


sub _del {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
    my $pages_path  = $self->{'conf'}->{'web'}->{'pages'};
    my $file        = lc $self->param('var');
    $file =~ s/\..*$//;
	if ( $db->select_one_to_hashref('title','pages',{title=>$file}) ) {
		if (-e $pages_path . $file . '.html') {
			unlink( $pages_path . $file . '.html' )
	          or die "Can\'t delete file ${pages_path}${file}.html: $!\n";
		}
        $db->delete('pages',{title => $file});
        return "<script>alert('成功删除页面 $file !');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
	}
	else {
		return "<script>alert('页面不存在!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
	}  
}
#######################


sub _restatic_all {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $result = $db->select_all_to_hashref('*','pages',{trashed => 0});
	for my $file (keys %{$result}) {
		$self->do_static($file,$result->{$file}->[0]);
	}
	return "<script>alert('重新生成静态页面成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
}
#######################


sub do_static {
    my ( $self, $file, $content ) = @_;
    return "你不是管理员!" unless $self->is_super; 
    my $pages_path = $self->{'conf'}->{'web'}->{'pages'};
    my $t          = $self->load_tmpl('admin_pages_do_static.tmpl');
    $t->param(
        HOME                => $self->{'conf'}->{'web'}->{'home'},
        TITLE               => "View $file",
        FILENAME            => $file,
        CONTENT             => $p->textile($content),
        CREATE_OR_EDIT_USER => $self->now('datetime') . ' by ' . $self->realname,
    );
    open( FILE, ">$pages_path$file.html" )
      or die "Can\'t open file $file: $!\n";
    print FILE $t->output;
    close FILE;
}
#######################


1;

