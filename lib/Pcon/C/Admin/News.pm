package Pcon::C::Admin::News;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use Text::Textile;
use base 'Pcon';

my $p = new Text::Textile( char_encoding => 0 );

sub hello {
	no strict 'subs';
	return __PACKAGE__;
}

sub _panel {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
    my $announcement = $db->select_one_to_hashref('*','announcement',{id =>1 });
    $db->select(
    	{
    		fields => ['datetime','title','userid','name'],
    		table => 'news',
    		order => 'datetime desc',
    	}
    );
    my $news = $db->fetchall_arrayref;
	my $t = $self->load_tmpl('admin_news_panel.tmpl');
	$t->param(
			HOME     => $self->{'conf'}->{'web'}->{'home'},
			ANNOUNCEMENT => $announcement->{'content'},
			NEWS => [
				map { 
					{ 
						HOME => $self->{'conf'}->{'web'}->{'home'},
						DATETIME => $_->[0],
						TITLE => $_->[1],
						USERID	=> scalar reverse ($_->[2]),
						NAME => $_->[3]
					 } 
				} @{$news}
			],
	);
	return $t->output;
}
########################


sub _announcement {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $q = $self->query;
	my $content = $q->param('content');
	$db->replace(
		'announcement',
		{
			id => 1,
			datetime => $content ? $self->now('datetime') : "",
			content => $content,
			userid => $self->authen->username,
			name => $db->select_one_to_hashref('name','users',{userid=>$self->authen->username})->{'name'}, 
		},
		{id=>1},
	);
	return "<script>alert('发布通知成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
}
########################


sub _add {
	my $self= shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $q = $self->query;
	if (defined $q->param('content') && $q->param('content')) {
		$db->insert(
			'news',
			{
				datetime => $self->now('datetime'),
				title => $p->encode_html($q->param('title')),
				content => $q->param('content'),
				userid => $self->authen->username,
				name => $db->select_one_to_hashref('name','users',{userid=>$self->authen->username})->{'name'}, 
			},
		);
	}
	
	if ( $db->rows ) {
		return "<script>alert('添加新闻成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
	}
	else {
		return "<script>alert('添加新闻失败!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>"
	}
}
########################


sub _edit {
	my $self= shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $datetime = $self->param('var');
	my $q = $self->query;
	$datetime =~ s/[^\d-:\s]//g;
	warn $datetime;
	if ($q->param('do_edit')) {
		$db->update(
			'news',
			{		
				datetime => $datetime,
				title => $q->param('title'),
				content => $q->param('content'),
				userid => $self->authen->username,
				name => $db->select_one_to_hashref('name','users',{userid=>$self->authen->username})->{'name'}, 
			},
			{
				datetime => $datetime,
			}
		);
		if ( $db->rows ) {
			return "<script>alert('修改新闻成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
		}
		else {
			return "<script>alert('修改新闻失败!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>"
		}
	}
	else {
		my $result = $db->select_one_to_hashref('*','news',{datetime => $datetime});
		if ($result) {
			my $t = $self->load_tmpl('admin_news_edit.tmpl');
			$t->param(
				HOME => $self->{'conf'}->{'web'}->{'home'},
				DATETIME => $datetime,
				TITLE => $result->{'title'},
				CONTENT => $result->{'content'},
				FORMATED_CONTENT => $p->textile($result->{'content'}),
			);
			return $t->output;
		}
		else {
			return "<script>alert('数据库中没有这条新闻!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>"
		}
	}
}
########################


sub _del {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $db = $self->{'dsn'};
	my $var = $self->param('var');
	$db->delete('news',{datetime => $var});
	if ( $db->rows ) {
		return "<script>alert('删除新闻成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>"
	}
	else {
	return "<script>alert('删除新闻失败!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>"
	}
}



1;
