package Pcon::C::Admin::Files;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'Pcon';

sub hello {
	no strict 'subs';
	return __PACKAGE__;
}

sub _panel {
	my $self = shift;
	return "你不是管理员!" unless $self->is_super; 
	my $t = $self->load_tmpl('admin_news_panel.tmpl');
	$t->param(
			HOME     => $self->{'conf'}->{'web'}->{'home'},
	);
	return $t->output;
}

1;
