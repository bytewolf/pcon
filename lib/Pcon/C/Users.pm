package Pcon::C::Users;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use Digest::MD5 qw/md5_hex/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'Pcon';

#######################
sub reg {
    my $self = shift;
    my $q    = $self->query();
    my $db   = $self->{'dsn'};
    return $self->reg_form unless $q->param('do_reg');

    my ( $results, $err_page ) =
      $self->check_rm( 'reg_form', 'reg_form_profile',
        { fill_password => 1, ignore_fields => ['check_sum'] } );
    return $err_page if $err_page;

    if (
        $db->select_one_to_hashref(
            'userid', 'users', { userid => $q->param('userid') }
        )
      )
    {
        $self->args_error(
'该用户已存在!<br><a href="javascript:history.go(-1);"><--- 返回</a>'
        );
    }
    else {
        my $ipaddr = $ENV{'HTTP_X_FORWARDED_FOR'} || $ENV{'REMOTE_ADDR'};
        $db->insert(
            'users',
            {
                userid     => $q->param('userid'),
                name       => $q->param('name'),
                passwd     => md5_hex( $q->param('passwd') ),
                phone      => $q->param('phone') || 'NULL',
                occupation => $q->param('occupation') || 'NULL',
                profile    => $q->param('profile') || 'NULL',
                attr       => $q->param('attr') || 0,
                ipaddr     => $ipaddr,
                date       => $self->now('date'),
            }
        );
        my $m = $self->load_tmpl('users_reg_mail.tmpl');
        $m->param(
            USERID     => $q->param('userid'),
            NAME       => $q->param('name'),
            PASSWD     => $q->param('passwd'),
            PHONE      => $q->param('phone') || 'NULL',
            OCCUPATION => $q->param('occupation') || 'NULL',
            PROFILE    => $q->param('profile') || 'NULL',
            DATE       => $self->now('date'),
        );
        eval {
            my $mailcfg = {
                to      => $q->param('userid'),
                subject => 'Registration  for YAPC::Beijing has been accepted!',
                msg     => $m->output,
            };
            $self->sendmail($mailcfg) or die "Mail sender error: $!\n";
        };

        my $t = $self->load_tmpl('users_reg_ok.tmpl');
        $t->param(
            HOME   => $self->{'conf'}->{'web'}->{'home'},
            USERID => $q->param('userid'),
            NAME   => $q->param('name'),
        );
        $self->header_props(-cookie=>[$q->cookie("hash"=>0)]);
        return $t->output;
    }

}

sub reg_form {
    my ( $self, $errs ) = @_;
    my $t = $self->load_tmpl( 'users_reg.tmpl', die_on_bad_params => 0 );
    $t->param( HOME => $self->{'conf'}->{'web'}->{'home'} );
    $t->param($errs) if $errs;
    return $t->output;
}

sub reg_form_profile {
    my $self = shift;
    my $q    = $self->query();
    my $name = $q->param('name');
    $name =~ s/\s+//g;
    $q->param( 'name',   $name );
    $q->param( 'userid', lc $q->param('userid') );

    return {
        required     => [qw/userid name passwd confirm_passwd check_sum attr/],
        optional     => [qw/phone occupation profile/],
        filters      => ['trim'],
        dependencies => { passwd => ['confirm_passwd'], },
        constraints  => {
            userid         => 'email',
            name           => '/^.{1,32}$/',
            passwd         => '/^[0-9a-zA-Z]{6,16}$/',
            confirm_passwd => sub {
                my $var = shift;
                return 1
                  if (  $var =~ /^[0-9a-zA-Z]{6,16}$/
                    and $var eq $q->param('passwd') );
                return 0;
            },
            check_sum => sub {
                return $self->captcha_verify( $q->cookie('hash'),
                    $q->param('check_sum') );
			},
            phone      => '/^[\d\-\+]{0,16}$/',
            occupation => '/^.{0,64}$/',
            attr       => '/^(audience|speaker|volunteer){1}$/',
            profile    => sub {
                my $var = shift;
                return 1 if $var =~ /^.{0,512}$/s;
                return 0;
            },
        },
    };

}

#######################
sub forgot_pw {
    my $self = shift;
    my $q    = $self->query();
    my $db   = $self->{'dsn'};
    return $self->forgot_pw_form unless $q->param('do_post');
    my ( $results, $err_page ) =
      $self->check_rm( 'forgot_pw_form', 'forgot_pw_form_profile',
        { ignore_fields => ['check_sum'] } );
    return $err_page if $err_page;
}

sub forgot_pw_form {
    my ( $self, $errs ) = @_;
    my $t = $self->load_tmpl( 'users_forgot_pw.tmpl', die_on_bad_params => 0 );
    $t->param( HOME => $self->{'conf'}->{'web'}->{'home'} );
    $t->param($errs) if $errs;
    return $t->output;
}

sub forgot_pw_form_profile {
    my $self = shift;
    my $q    = $self->query();
    $q->param( 'userid', lc $q->param('userid') );
    return {
        required => [qw/userid/],
        filters  => ['trim'],
    };

}

#######################
sub list {
    my $self   = shift;
    my $db     = $self->{'dsn'};
    my $result = "";
    my $t      = $self->load_tmpl('users_list.tmpl');
    $db->select(
        {
            fields => [ 'userid', 'name', 'attr', 'super' ],
            table  => 'users',
            order  => 'date',
        }
    );
    $result = $db->fetchall_arrayref;

    my ( @speaker, @audience, @volunteer, @super );
    if ( ref $result eq 'ARRAY' && @{$result} ) {
        for ( @{$result} ) {

            push @super => {
                USERID => scalar reverse( $_->[0] ),
                NAME   => $_->[1],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              }
              if $_->[3];

            push @speaker => {
                USERID => scalar reverse( $_->[0] ),
                NAME   => $_->[1],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              }
              and next
              if $_->[2] eq 'speaker';

            push @audience => {
                USERID => scalar reverse( $_->[0] ),
                NAME   => $_->[1],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              }
              and next
              if $_->[2] eq 'audience';

            push @volunteer => {
                USERID => scalar reverse( $_->[0] ),
                NAME   => $_->[1],
                HOME   => $self->{'conf'}->{'web'}->{'home'},
              }
              and next
              if $_->[2] eq 'volunteer';

        }
    }

    $t->param(
        HOME      => $self->{'conf'}->{'web'}->{'home'},
        SPEAKER   => \@speaker,
        AUDIENCE  => \@audience,
        VOLUNTEER => \@volunteer,
        SUPER     => \@super,
    );
    return $t->output;
}

#######################
sub view {
    my $self   = shift;
    my $userid = scalar reverse $self->param('var');
    my $db     = $self->{'dsn'};
    my $result = "";
    my %attr = (
    	audience => '听众',
    	speaker => '演讲者',
    	volunteer => '志愿者'
    );
    if ( defined $userid && $userid ne "" ) {
        $result =
          $db->select_one_to_hashref( '*', 'users', { userid => $userid } );
    }
    else {
        $result = $db->select_all_to_hashref( '*', 'users' );
    }
    
    return $self->args_error("$userid not exist!") unless $result;
    
    if ( $self->is_super ) {
    	$result->{'phone'} = "perl -e \' print scalar reverse q/ ".scalar(reverse $result->{'phone'}) ." / \ '";
    } 
    elsif ( $result->{'phone'} ne 'NULL' ) {
	$result->{'phone'} = "请询问大会管理人员:-)!";
    }
    else{
    	$result->{'phone'} = "NULL";
    }
    
    my $t = $self->load_tmpl('users_view.tmpl');
    $t->param(
    	NAME => $result->{'name'},
    	USERID => "perl -e \' print scalar reverse q/ ".scalar(reverse $result->{'userid'} )." / \ '",
    	PHONE => $result->{'phone'} ,
    	OCCUPATION => $result->{'occupation'},
    	PROFILE => $result->{'profile'},
    	ATTR => $attr{ $result->{'attr'} },
    );
    return $t->output;
}

#######################
sub _edit {
	my $self = shift;
	my $q = $self->query();
	my $db = $self->{'dsn'};
	my ($results,$err_page) = $self->check_rm('edit_form','edit_form_profile',{ignore_fields => ['check_sum']});
	return $err_page if $err_page;
	$db->update(
		'users',
		{
			name => $q->param('name'),
			phone => $q->param('phone'),
			occupation => $q->param('occupation'),
			profile => $q->param('profile'),
			attr => $q->param('attr'),
		},
		{userid => $self->authen->username}, 
	);

	return "<script>alert('修改资料成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
}

sub edit_form {
	my ($self,$errs) = @_;
    my $t = $self->load_tmpl( 'users_manage.tmpl', die_on_bad_params => 0 );
    $t->param( HOME => $self->{'conf'}->{'web'}->{'home'} );
    $t->param($errs) if $errs;
    return $t->output;	
}

sub edit_form_profile {
    my $self = shift;
    my $q    = $self->query();
    $q->param( 'userid', $self->authen->username );

    return {
        required     => [qw/userid name check_sum attr/],
        optional     => [qw/phone occupation profile/],
        filters      => ['trim'],
        constraints  => {
            userid         => 'email',
            name           => '/^.{1,32}$/',
            check_sum => sub {
                return $self->captcha_verify( $q->cookie('hash'),
                    $q->param('check_sum') );
			},
            phone      => '/^[\d\-\+]{0,16}$/',
            occupation => '/^.{0,64}$/',
            attr       => '/^(audience|speaker|volunteer){1}$/',
            profile    => sub {
                my $var = shift;
                return 1 if $var =~ /^.{0,512}$/s;
                return 0;
            },
        },
    };

	
}


#######################
sub _edit_passwd {
    my $self = shift;
	my $q = $self->query();
	my $db = $self->{'dsn'};
    my $result =
      $db->select_one_to_hashref( '*', 'users',
        { userid => $self->authen->username } );
    my $is_super   = $result->{'super'};
	my $t = $self->load_tmpl('users_edit_passwd.tmpl');
	$t->param(
        HOME                => $self->{'conf'}->{'web'}->{'home'},
        USERID              => $self->authen->username,	
	);
	return $t->output unless $q->param('do_edit_passwd');
	my ($results,$err_page) = $self->check_rm('edit_passwd_form','edit_passwd_form_profile',{ignore_fields => ['check_sum']});
	return $err_page if $err_page;
	$db->update(
		'users',
		{
			passwd => md5_hex($q->param('new_passwd')),
		},
		{userid => $self->authen->username},
	);
	return "<script>alert('修改密码成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";


}

sub edit_passwd_form {
	my ($self,$errs) = @_;
    my $t = $self->load_tmpl( 'users_edit_passwd.tmpl', die_on_bad_params => 0 );
    $t->param( HOME => $self->{'conf'}->{'web'}->{'home'} );
    $t->param($errs) if $errs;
    return $t->output;	
}

sub edit_passwd_form_profile {
    my $self = shift;
	my $db = $self->{'dsn'};
    my $q    = $self->query();
    $q->param( 'userid', $self->authen->username );
	my ($old_passwd) = $db->select_one_to_array('passwd','users',{userid => $self->authen->username});
	warn "passwd ".$q->param('passwd')."ppp $old_passwd===\n";
    return {
        required     => [qw/userid passwd new_passwd confirm_new_passwd check_sum/],
        filters      => ['trim'],
        constraints  => {
			passwd => sub{
				my $var = shift;
				return 1 if (
					$var =~ /^[0-9a-zA-Z]{6,16}$/
						and md5_hex($var) eq $old_passwd
				);
				return 0;
			},
			new_passwd => '/^[0-9a-zA-Z]{6,16}$/',
			confirm_new_passwd => sub {
				my $var = shift;
				return 1 if (
					$var =~ /^[0-9a-zA-Z]{6,16}$/
						and $var eq $q->param('new_passwd')
				);
				return 0;
			},
			check_sum => sub {
        		return $self->captcha_verify( $q->cookie('hash'),
            	$q->param('check_sum') );
			},
        },
    };


}

#######################
sub login {
    my $self = shift;
    $self->redirect( $self->{'conf'}->{'web'}->{'home'} . '/authen_login' );
}

#######################
sub logout {
    my $self = shift;
    $self->authen->logout;
    $self->redirect('/html/logout.html');
}

#######################
sub _manage {
    my $self   = shift;
    my $t      = $self->load_tmpl('users_manage.tmpl');
    my $db     = $self->{'dsn'};
    my $result =
     $db->select_one_to_hashref( '*', 'users',
       { userid => $self->authen->username } );
 
    $t->param(
        HOME       => $self->{'conf'}->{'web'}->{'home'},
        USERID     => $self->authen->username,
        NAME       => $result->{'name'},
        OCCUPATION => $result->{'occupation'},
        PROFILE    => $result->{'profile'},
        PHONE      => $result->{'phone'},
        AUDIENCE   => $result->{'attr'} eq 'audience' ? 1 : 0,
        SPEAKER    => $result->{'attr'} eq 'speaker' ? 1 : 0,
        VOLUNTEER  => $result->{'attr'} eq 'volunteer' ? 1 : 0,

    );
    return $t->output;
}

1;
