package Pcon::Tools;

use strict;
use Data::Dumper;
use Mail::Sender;

sub redirect {
    my ( $self, $url ) = @_;
    $self->header_type('redirect');
    return $self->header_props( -url => $url );
}

sub sendmail {
    my ( $self, $mailcfg ) = @_;
    my $sender = new Mail::Sender {
        smtp => $self->{'conf'}->{'mail'}->{'smtp'},
        port => $self->{'conf'}->{'mail'}->{'port'},
        from => $mailcfg->{'from'} || $self->{'conf'}->{'mail'}->{'from'},
        ctype => 'text/plain',
        encoding => '8bit',
        charset => $self->{'conf'}->{'mail'}->{'charset'},
    };
    $sender->MailMsg($mailcfg);

}

sub args_error {
    my ( $self, $msg ) = @_;
    my $result = <<"HTML";
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>Oops!</title>
		</head>
		<body>
			<b>$msg</b>
		</body>
	</html>
HTML
    return $result;
}

sub now {
    my ( $self, $date_or_datetime ) = @_;
    my ( $sec, $min, $hour, $day, $mon, $year ) = localtime(time);
    $mon += 1;
    $year = $year + 1900;
    $mon  = "0$mon" if $mon < 10;
    $day  = "0$day" if $day < 10;
    $hour = "0$hour" if $hour < 10;
    $min  = "0$min" if $min < 10;
    $sec  = "0$sec" if $sec < 10;
    if ( $date_or_datetime eq 'datetime' ) {
        return "$year-$mon-$day $hour:$min:$sec";
    }
    elsif ( $date_or_datetime eq 'date' ) {
        return "$year-$mon-$day";
    }
    elsif ( $date_or_datetime eq 'time' ) {
        return "$hour:$min:$sec";
    }
    else {
        return "$year-$mon-$day";
    }
}

1;
