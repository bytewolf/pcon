package Pcon;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use Data::Dumper;
use base qw/CGI::Application Pcon::Tools/;
use HTML::Entities;
use Text::Textile;
use CGI::Carp qw/fatalsToBrowser/;
use CGI::Application::Plugin::Session;
use CGI::Application::Plugin::CAPTCHA;
use CGI::Application::Plugin::ValidateRM;
use CGI::Application::Plugin::Config::YAML;
use CGI::Application::Plugin::Authentication;
use Pcon::Conf;
use Pcon::M::DBIx;
use Pcon::V::Template;

my $p = new Text::Textile( char_encoding => 0 );

sub cgiapp_init {
    my $self = shift;

    #############
    # DSN initialize
    #############
    $self->{'dsn'} = $self->get_dsn( $self->get_conf );

    #############
    # Authen initialize
    #############
    my $post_login_to = $self->query->param('destination');
    if ( $post_login_to =~ /authen_login/ ) {
        $post_login_to = $self->{'conf'}->{'web'}->{'home'};
    }
    elsif ( $post_login_to eq "" ) {
        $post_login_to = $self->{'conf'}->{'web'}->{'home'};
    }

    $self->authen->config(
        DRIVER => [
            'DBI',
            DBH         => $self->{'dsn'}->{'dbh'},
            TABLE       => 'users',
            CONSTRAINTS => {
                'userid'         => '__CREDENTIAL_1__',
                'md5_hex:passwd' => '__CREDENTIAL_2__'
            },
        ],
        STORE                 => 'Session',
        LOGIN_SESSION_TIMEOUT => {
            IDLE_FOR => '30m',
            EVERY    => '1d',
        },
        LOGIN_FORM => {
            TITLE                => '登录',
            USERNAME_LABEL       => '用户Email',
            PASSWORD_LABEL       => '用户密码',
            REMEMBERUSER_LABEL   => '保存登录名?',
            FORGOTPASSWORD_LABEL => '回首页',
            SUBMIT_LABEL         => '登录',
            BASE_COLOUR          => '#000000',
            LIGHTER_COLOUR       => '#FFFFFF',
            DARK_COLOUR          => '#000000',
            LIGHT_COLOUR         => '#FFFFFF',
            DARKER_COLOUR        => '#FFFFFF',
            FORGOTPASSWORD_URL   => $self->{'conf'}->{'web'}->{'home'},
        },
        POST_LOGIN_URL => $post_login_to,
    );
    $self->authen->protected_runmodes(qr/^_/);

    ###############
    # Form Validate initialize
    ###############
    $self->param('dfv_defaults')
      || $self->param(
        'dfv_defaults',
        {
            missing_optional_valid => 1,
            filters                => 'trim',
            msgs                   => {
                any_errors => 'err__',
                prefix     => 'err_',
                invalid    => '* Error',
                missing    => '* Required',
                format     => '<font color=red>%s</font>',
            },
        }
      );
}

sub cgiapp_get_query {
    my $self = shift;
    require CGI;
    $CGI::POST_MAX = $self->{'conf'}->{'web'}->{'uploadmaxsize'};
    my $q = CGI->new();
    return $q;
}

sub cgiapp_prerun {
    my $self = shift;

}

sub cgiapp_postrun {
    my $self = shift;
    $self->header_add( -charset => 'UTF-8' );
}

sub setup {
    my $self = shift;
    $self->start_mode('view');
    $self->mode_param('rm');
    $self->run_modes( $self->get_runmodes );
    $self->tmpl_path( $self->{'conf'}->{'web'}->{'templates'} );
}

sub teardown {
    my $self = shift;
    $self->{'dsn'}->finish;
    $self->{'dsn'}->disconnect;
}

sub view {
    my $self = shift;
    my $db     = $self->{'dsn'};
    my $result =
      $db->select_one_to_hashref( '*', 'users',{ userid => $self->authen->username } );
    my $announcement = $db->select_one_to_hashref('*','announcement',{id =>1 });
    $db->select(
    	{
    		fields => ['datetime','title','content','name','userid'],
    		table => 'news',
    		order => 'datetime desc',
    	}
    );
    my $news = $db->fetchall_arrayref;
	my @news = @{$news};
    my $t = $self->load_tmpl('index.tmpl');
    $t->param(
        HOME     => $self->{'conf'}->{'web'}->{'home'},
        IS_LOGIN => $self->authen->username ? 1 : 0,
        IS_SUPER => $result->{'super'},
        USERID   => $self->authen->username,
        ANNOUNCEMENT => $p->textile($announcement->{'content'}),
        ANNOUNCEMENT_DATETIME =>  $announcement->{'datetime'},
        ANNOUNCEMENT_USERID => scalar reverse ($announcement->{'userid'}),
        ANNOUNCEMENT_NAME => $announcement->{'name'},
        NEWS => [
			map { 
				{ 
			        HOME => $self->{'conf'}->{'web'}->{'home'},
					USERID => scalar reverse ($_->[4]),
					NAME => $_->[3],
					DATETIME => $_->[0],
					TITLE => $_->[1],
					CONTENT =>  $p->textile($_->[2]),
				 } 
			} @news
		],
    );
    return $t->output;
}

sub is_super {
	my $self = shift;
    my $db     = $self->{'dsn'};
    my $result =
      $db->select_one_to_hashref( 'super', 'users',{ userid => $self->authen->username } );
    return $result->{'super'} ? 1 : 0;
}

sub realname {
	my $self = shift;
    my $db     = $self->{'dsn'};
    my $result =
      $db->select_one_to_hashref( 'name', 'users',{ userid => $self->authen->username } );
    return $result->{'name'};
}

1;
